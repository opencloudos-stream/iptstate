Summary:  A top-like interface to netfilter connection-tracking table
Name: iptstate
Version: 2.2.7
Release: 6%{?dist}
License: zlib
URL: http://www.phildev.net/iptstate/
Source0: https://github.com/jaymzh/iptstate/releases/download/v%{version}/iptstate-%{version}.tar.bz2

# Patch0001 is from fedora
Patch0001: iptstate-2.1-man8.patch

BuildRequires: gcc-c++ make
BuildRequires: ncurses-devel libnetfilter_conntrack-devel
Requires: iptables

%description
IP Tables State (iptstate) was originally written to implement 
the "state top" feature of IP Filter in IP Tables. "State top" 
displays the states held by your stateful firewall in a top-like 
manner.Since IP Tables doesn't have a built in way to easily display 
this information even once, an option was added to just have it 
display the state table once.
Features include:
  - Top-like realtime state table information
  - Sorting by any field
  - Reversible sorting
  - Single display of state table
  - Customizable refresh rate
  - Display filtering
  - Color-coding
  - Open Source
  - much more...


%prep
%autosetup -p1


%build
%make_build CXXFLAGS="%{optflags} %{build_ldflags}"


%install
%make_install PREFIX=%{buildroot}%{_prefix} 


%files
%license LICENSE 
%doc README.md
%{_sbindir}/iptstate
%{_mandir}/man8/iptstate.8.gz


%changelog
* Thu Sep 26 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.2.7-6
- Rebuilt for clarifying the packages requirement in BaseOS and AppStream

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.2.7-5
- Rebuilt for loongarch release

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.2.7-4
- Rebuilt for OpenCloudOS Stream 23.09

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.2.7-3
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.2.7-2
- Rebuilt for OpenCloudOS Stream 23

* Tue Jul 19 2022 doriscchao <doriscchao@tencent.com> 2.2.7-1
- initial build, Version 2.2.7, Release 1
